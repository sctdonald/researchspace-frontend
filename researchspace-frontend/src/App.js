import logo from './logo.svg';
import './App.css';
import {Router} from 'react-router-dom';
import React from "react";
import AppRouter from "./ui/AppRouter";
import {createBrowserHistory} from 'history';

let browserHistory = createBrowserHistory();

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Router history={browserHistory}>
            <AppRouter />
        </Router>
      </header>
    </div>
  );
}

export default App;
