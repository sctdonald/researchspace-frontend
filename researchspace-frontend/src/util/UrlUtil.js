//@flow
import _ from 'lodash';

export type QueryParams = { [string]: string }

export default class UrlUtil {

    static getQueryParams(): QueryParams {
        let result = _.chain(window.location.href).split('#').nth(0).split('?').nth(1).split('&').map(_.partial(_.split, _, '=', 2)).fromPairs().value();
        delete result[""];
        return result;
    }
}