export type ContentSummary = {
    totalCount: number,
    subSampleCount: number,
    containerCount: number
}