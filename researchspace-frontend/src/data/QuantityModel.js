export type Quantity = {
    numericValue: number,
    unitId: number
}