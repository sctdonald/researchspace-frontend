import type {Link} from "./LinkModel";

export type Owner = {
    id: number,
    username: string,
    email: string,
    firstName: string,
    lastName: string,
    homeFolderId: number,
    links: Array<Link>
}