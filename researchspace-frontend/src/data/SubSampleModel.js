import type {Owner} from "./OwnerModel";
import type {Quantity} from "./QuantityModel";
import type {Link} from "./LinkModel";
import type {ParentContainer} from "./ParentContainerModel";
import type {Sample} from "./SampleModel";

export type SubSample = {
    id: number,
    globalId: string,
    name: string,
    description: string,
    created: string,
    createdBy: string,
    lastModified: string,
    modifiedBy: string,
    modifiedByFullName: string,
    deleted: string,
    deletedDate: string,
    revisionId: string,
    owner: Owner,
    permittedActions: Array<String>,
    iconId: number,
    quantity: Quantity,
    tags: string,
    barcode:string,
    type: string,
    links: Array<Link>,
    parentContainers:ParentContainer,
    lastMoveDate: string,
    sample: Sample
}

export type UserSubSamples = {
    totalHits: number,
    pageNumber: number,
    subSamples: Array<SubSample>,
    links: Array<Link>
}

export type SubSampleDetailInfo = {
    id: number,
    globalId: string,
    name: string,
    description: string,
    created: string,
    createdBy: string,
    lastModified: string,
    modifiedBy: string,
    modifiedByFullName: string,
    deleted: string,
    deletedDate: string,
    revisionId: string,
    owner: Owner,
    permittedActions: Array<String>,
    iconId: number,
    quantity: Quantity,
    tags: string,
    barcode:string,
    type: string,
    links: Array<Link>,
    expiryDate: string,
    notes: Array<string>,
    extraFields: Array<string>,
    sample: Sample
}