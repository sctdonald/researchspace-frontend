import type {Link} from "./LinkModel";
import type {Owner} from "./OwnerModel";
import type {Quantity} from "./QuantityModel";
import type {SubSample} from "./SubSampleModel";

export type Sample = {
    id: number,
    globalId: string,
    name: string,
    description: string,
    created: string,
    createdBy: string,
    lastModified: string,
    modifiedBy: string,
    modifiedByFullName: string,
    deleted: string,
    deletedDate: string,
    revisionId: string,
    owner: Owner,
    permittedActions: Array<String>,
    iconId: number,
    quantity: Quantity,
    tags: string,
    barcode:string,
    type: string,
    links: Array<Link>,
    version: number,
    historicalVersion: boolean,
    templateId: number,
    templateVersion: number,
    template: boolean,
    subSampleAlias: string,
    subSamplesCount: number,
    storageTempMin: number ,
    storageTempMax: number,
    sampleSource: string,
    expiryDate: string
}

export type UserSamples = {
    totalHits: number,
    pageNumber: number,
    samples: Array<Sample>,
    links: Array<Link>
}

export type SampleDetailInfo = {
    id: number,
    globalId: string,
    name: string,
    description: string,
    created: string,
    createdBy: string,
    lastModified: string,
    modifiedBy: string,
    modifiedByFullName: string,
    deleted: string,
    deletedDate: string,
    revisionId: string,
    owner: Owner,
    permittedActions: Array<String>,
    iconId: number,
    quantity: Quantity,
    tags: string,
    barcode:string,
    type: string,
    links: Array<Link>,
    version: number,
    historicalVersion: boolean,
    templateId: number,
    templateVersion: number,
    template: boolean,
    subSampleAlias:string,
    subSamplesCount:string,
    storageTempMin: number,
    storageTempMax: number,
    sampleSource:string,
    expiryDate: string,
    fields: Array<string>,
    extraFields: Array<string>,
    subSamples: Array<SubSample>
}