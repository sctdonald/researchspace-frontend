export type Link = {
    link: string,
    rel: string
}