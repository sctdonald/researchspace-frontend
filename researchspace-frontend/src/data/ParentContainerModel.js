import type {Owner} from "./OwnerModel";
import type {Quantity} from "./QuantityModel";
import type {Link} from "./LinkModel";
import type {ContentSummary} from "./ContentSummaryModel";

export type ParentLocation = {
    id: number,
    coordX: number,
    coordY:number
}

export type ParentContainer = {
    id: number,
    globalId: string,
    name: string,
    description: string,
    created: string,
    createdBy: string,
    lastModified: string,
    modifiedBy: string,
    modifiedByFullName: string,
    deleted: string,
    deletedDate: string,
    revisionId: string,
    owner: Owner,
    permittedActions: Array<String>,
    iconId: number,
    quantity: Quantity,
    tags: string,
    barcode:string,
    type: string,
    links: Array<Link>,
    locationsCount: number,
    contentSummary: ContentSummary,
    cType:string,
    canStoreSamples:boolean,
    canStoreContainers:boolean,
    parentLocation:ParentLocation,
    lastMoveDate: string
}