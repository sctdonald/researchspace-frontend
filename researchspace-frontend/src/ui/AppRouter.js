//@flow

import React, {Component} from 'react';
import {
    Route,
    Switch
} from "react-router-dom";
import LandingPage from "./component/LandingPage";
import SearchSample from "./component/sample/SearchSample";
import SearchSubSample from "./component/subsample/SearchSubSample";
import SampleDetail from "./component/sample/SampleDetail";
import SubSampleDetail from "./component/subsample/SubSampleDetail";

type Props = {};

class AppRouter extends Component<Props> {
    render() {
        return (
            <Switch>
                <Route exact path={"/"} component={LandingPage}/>
                <Route exact path={"/sample"} component={SearchSample}/>
                <Route exact path={"/sampleDetail"} component={SampleDetail}/>
                <Route exact path={"/subSample"} component={SearchSubSample}/>
                <Route exact path={"/SubSampleDetail"} component={SubSampleDetail}/>
            </Switch>
        )
    }
}

export default AppRouter;