//@flow
import React, {Component} from 'react';

export default class LandingPage extends Component{
    render() {
        return (
            <div>
                Welcome to the ResearchSpace Landing Page. As a lab technical, you may search relevant sample report in this site.
                <br />
                You may now choose the following type to search:
                <br />
                <a href={"/sample"}>Sample</a>
                <br />
                <a href={"/subSample"}>SubSample</a>
            </div>
        );
    }
}