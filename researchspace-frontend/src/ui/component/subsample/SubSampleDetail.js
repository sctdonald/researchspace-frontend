//@flow

import React, {Component} from "react";
import UrlUtil from "../../../util/UrlUtil";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import type {SubSampleDetailInfo} from "../../../data/SubSampleModel";
import Config from "../../../env/Config";

type Props = {}
type State = {
    subSampleDetail: SubSampleDetailInfo,
    currentSubSampleId: number,
    subSampleIdSearch: number,
    isLoading: boolean
}

export default class SubSampleDetail extends Component<Props, State>{
    constructor(props) {
        super(props);
        this.state = ({subSampleDetail: null, isLoading: false});
        this.fetchSubSampleDetail = this.fetchSubSampleDetail.bind(this);
        this.onSubSampleIdChange = this.onSubSampleIdChange.bind(this);
        this.onSearchSubSampleDetailButtonClick = this.onSearchSubSampleDetailButtonClick.bind(this);
    }

    componentDidMount(): void {
        let queryParams = UrlUtil.getQueryParams();
        let subSampleId = queryParams["id"];
        console.log(subSampleId);
        this.setState({currentSubSampleId: subSampleId, subSampleIdSearch: subSampleId})
    }

    componentDidUpdate(prevProps: Props, prevState: State, prevContext: *): * {
        if (this.state.currentSubSampleId !== prevState.currentSubSampleId) {
            this.setState({subSampleDetail: null, isLoading: true})
            this.fetchSubSampleDetail();
        }
    }

    fetchSubSampleDetail() {
        fetch(Config.researchBackend+"/v1/subSample/" + this.state.subSampleIdSearch + "/get-subSample-detail",
            {
                method: "GET",
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Request-Method': '*'
                }
            })
            .then(res => {
                if (!res.ok) {
                    this.setState({
                        isLoading: false,
                        subSampleDetail: null,
                        error: "Inventory record with id " + this.state.subSampleIdSearch + " could not be retrieved - possibly it has been deleted, does not exist, or you do not have permission to access it."
                    })
                }
                return res.json()
            })
            .then(
                (result) => {
                    if (!result.status) {
                        this.setState({
                            isLoading: false,
                            subSampleDetail: result
                        });
                    }
                }
            ).catch(function (error) {                        
            console.log('Request failed', error);
        })
    }

    onSubSampleIdChange(e) {
        this.setState({subSampleIdSearch: e.target.value});
    }

    onSearchSubSampleDetailButtonClick() {
        window.location.href = "/subSampleDetail?id=" + this.state.subSampleIdSearch;
    }


    render() {
        return (
            <div>
                <div>
                    <a className="refLink" href={"/"}>Home</a>
                    <a className="refLink" href={"/sample"}>Search By User Name</a>
                </div>
                {
                    !this.state.currentSubSampleId &&
                    <div>
                        <div>You may search sub sample by id in the following box,</div>
                        <div><input type={"number"} onChange={this.onSubSampleIdChange}
                                        value={this.state.subSampleIdSearch}/>
                            <button onClick={this.onSearchSubSampleDetailButtonClick}>go</button>
                        </div>
                    </div>
                }
                {
                    this.state.isLoading && <div>Loading...</div>
                }
                {
                    !this.state.isLoading && this.state.subSampleDetail &&
                    <div className="subSampleDetail">
                        <div>You are searching the sub sample detail with id: {this.state.currentSubSampleId}</div>
                        <div>ID: <input type={"number"} onChange={this.onSubSampleIdChange}
                                        value={this.state.subSampleIdSearch}/>
                            <button onClick={this.onSearchSubSampleDetailButtonClick}>go</button>
                        </div>
                        {
                            !this.state.isLoading && this.state.error &&
                            <div>
                                We got an error when searching the result, the error message as follow:
                                <br/>
                                {this.state.error}
                            </div>
                        }
                        {
                            !this.state.isLoading && this.state.subSampleDetail &&
                            <div>
                                <div>Global ID: {this.state.subSampleDetail.globalId}</div>
                                <div>Name: {this.state.subSampleDetail.name}</div>
                                <div>Description: {this.state.subSampleDetail.description}</div>
                                <div>Created: {this.state.subSampleDetail.created}</div>
                                <div>Created By: {this.state.subSampleDetail.createdBy}</div>
                                <div>Last Modified: {this.state.subSampleDetail.lastModified}</div>
                                <div>Modified By: {this.state.subSampleDetail.modifiedBy}</div>
                                <div>Modified By Full Name: {this.state.subSampleDetail.modifiedByFullName}</div>
                                <div>Deleted: {this.state.subSampleDetail.deleted}</div>
                                <div>Deleted Date: {this.state.subSampleDetail.deletedDate}</div>
                                {this.state.subSampleDetail.owner &&
                                <div>
                                    <div>Owner Id: {this.state.subSampleDetail.owner.id}</div>
                                    <div>Owner UserName: {this.state.subSampleDetail.owner.username}</div>
                                    <div>Owner Full
                                        Name: {this.state.subSampleDetail.owner.firstName} {this.state.subSampleDetail.owner.lastName}</div>
                                </div>
                                }
                                <div>Permitted Actions: {this.state.subSampleDetail.permittedActions.map(item => {
                                    return <div className="inlineText" key={item}>{item},</div>
                                })}</div>
                                <div>Tags: {this.state.subSampleDetail.tags}</div>
                                <div>Barcode: {this.state.subSampleDetail.barcode}</div>
                                <div>Type: {this.state.subSampleDetail.type}</div>
                                <div>Notes: {this.state.subSampleDetail.notes.map(item => {
                                    return <div className="inlineText" key={item}>{item}</div>
                                })}</div>
                                <div>Extra Fields: {this.state.subSampleDetail.extraFields.map(item => {
                                    return <div className="inlineText" key={item}>{item}</div>
                                })}</div>
                                <div>Sample:</div>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>id</TableCell>
                                                <TableCell align="right">Global Id</TableCell>
                                                <TableCell align="right">Sample Name</TableCell>
                                                <TableCell align="right">Description</TableCell>
                                                <TableCell align="right">Created By</TableCell>
                                                <TableCell align="right">Created At</TableCell>
                                                <TableCell align="right">Version</TableCell>
                                                <TableCell align="right">Expiry Date</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {this.state.subSampleDetail && this.state.subSampleDetail.sample &&
                                                <TableRow >
                                                    <TableCell component="th" scope="row"><a
                                                        href={"/sampleDetail?id=" + this.state.subSampleDetail.sample.id}>{this.state.subSampleDetail.sample.id}</a></TableCell>
                                                    <TableCell align="right">{this.state.subSampleDetail.sample.globalId}</TableCell>
                                                    <TableCell align="right">{this.state.subSampleDetail.sample.name}</TableCell>
                                                    <TableCell align="right">{this.state.subSampleDetail.sample.description}</TableCell>
                                                    <TableCell align="right">{this.state.subSampleDetail.sample.createdBy}</TableCell>
                                                    <TableCell align="right">{this.state.subSampleDetail.sample.created}</TableCell>
                                                    <TableCell align="right">{this.state.subSampleDetail.sample.version}</TableCell>
                                                    <TableCell align="right">{this.state.subSampleDetail.sample.expiryDate}</TableCell>
                                                </TableRow>
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        }

                    </div>
                }

            </div>
        );
    }
}