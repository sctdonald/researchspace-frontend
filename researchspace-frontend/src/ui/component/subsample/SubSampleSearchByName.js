//@flow

import {Component} from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import type {UserSubSamples} from "../../../data/SubSampleModel";
import Config from "../../../env/Config";

type Props = {}

type State = {
    searchName: string,
    isLoading: boolean,
    userSubSampleDetail: UserSubSamples,
    currentPage: number,
    error: string,
    isNameChanged: boolean
}


export default class SubSampleSearchByName extends Component<Props, State>{
    constructor(props) {
        super(props);
        this.state = ({
            searchName: "",
            isLoading: false,
            userSubSampleDetail: null,
            error: null,
            currentPage: 1,
            isNameChanged: false
        });
        this.onNameInput = this.onNameInput.bind(this);
        this.onSearchButtonClick = this.onSearchButtonClick.bind(this);
        this.onPageNumberChange = this.onPageNumberChange.bind(this);
    }

    onNameInput(e) {
        this.setState({searchName: e.target.value, isNameChanged: true});
    }

    onPageNumberChange(e) {
        this.setState({currentPage: e.target.value});
    }

    checkInsertPageNumber() {
        return !(Math.ceil(this.state.userSubSampleDetail.totalHits / 20) < this.state.currentPage || this.state.currentPage === 0);
    }

    onSearchButtonClick() {
        let isPageCorrect = true;
        if (!this.state.isNameChanged && this.state.userSubSampleDetail && !this.checkInsertPageNumber()) {
            isPageCorrect = false;
        }
        if (isPageCorrect && this.state.searchName) {
            this.setState({isLoading: true, error: null})
            fetch(Config.researchBackend+"/v1/subSample/user/" + this.state.searchName + "/get-all-subSamples?page=" + (this.state.currentPage - 1) + "&orderBy=name asc",
                {
                    method: "GET",
                    headers: {
                        'Content-type': 'application/json',
                        'Access-Control-Request-Method': '*'
                    }
                })
                .then(res => {
                    return res.json()
                })
                .then(
                    (result) => {
                        this.setState({
                            isLoading: false,
                            userSubSampleDetail: result,
                            isNameChanged: false
                        });
                    },
                    (error) => {
                        this.setState({
                            isLoading: false,
                            isNameChanged: false,
                            error
                        });
                    }
                )
        } else {
            if (!isPageCorrect) {
                this.setState({error: "Please enter the correct page number, range starting from 1 and cannot larger than the max page you have."})
            } else {
                this.setState({error: "Please enter the name before you search."})
            }
        }
    }

    render() {
        return (
            <div>
                <div>
                    <a className="refLink" href={"/"}>Home</a>
                    <a className="refLink" href={"/subSampleDetail"}>Search By SubSample ID</a>
                </div>

                You are searching the sub samples that visible to the username entered.
                <br/>
                <input type={"text"} onChange={this.onNameInput}/>
                <button onClick={this.onSearchButtonClick}>search</button>
                <br/>
                <br/>
                {
                    this.state.isLoading && <div>Loading...</div>
                }
                {
                    !this.state.isLoading && this.state.userSubSampleDetail && this.state.userSubSampleDetail.totalHits === 0 &&
                    <div>
                        No sample can be seen by the searched name.
                    </div>
                }
                {
                    !this.state.isLoading && this.state.userSubSampleDetail && this.state.userSubSampleDetail.totalHits !== 0 &&
                    <div className="box">
                        Here is the result of the search:
                        <br/>
                        Total number of Sample visible by the user: {this.state.userSubSampleDetail.totalHits}
                        <br/>
                        Current Page: <input onChange={this.onPageNumberChange} value={this.state.currentPage}
                                             type={"number"}/>/{this.state.userSubSampleDetail.totalHits % 20 === 0 ? this.state.userSubSampleDetail.totalHits / 20 : Math.ceil(this.state.userSubSampleDetail.totalHits / 20)}
                        <button onClick={this.onSearchButtonClick}>go</button>
                        <br/>
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>id</TableCell>
                                        <TableCell align="right">Global Id</TableCell>
                                        <TableCell align="right">Sample Name</TableCell>
                                        <TableCell align="right">Description</TableCell>
                                        <TableCell align="right">Created By</TableCell>
                                        <TableCell align="right">Created At</TableCell>
                                        <TableCell align="right">Version</TableCell>
                                        <TableCell align="right">Expiry Date</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.userSubSampleDetail && this.state.userSubSampleDetail.subSamples.map((row) => (
                                        <TableRow key={row.id}>
                                            <TableCell component="th" scope="row"><a
                                                href={"/subSampleDetail?id=" + row.id}>{row.id}</a></TableCell>
                                            <TableCell align="right">{row.globalId}</TableCell>
                                            <TableCell align="right">{row.name}</TableCell>
                                            <TableCell align="right">{row.description}</TableCell>
                                            <TableCell align="right">{row.createdBy}</TableCell>
                                            <TableCell align="right">{row.created}</TableCell>
                                            <TableCell align="right">{row.version}</TableCell>
                                            <TableCell align="right">{row.expiryDate}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                }
                {
                    !this.state.isLoading && this.state.error &&
                    <div>
                        We got an error when searching the result, the error message as follow:
                        <br/>
                        {this.state.error}
                    </div>
                }
            </div>
        );
    }
}