//@flow

import React, {Component} from 'react';
import SubSampleSearchByName from "./SubSampleSearchByName";

type State = {
    searchingType: string
}

type Props = {

}


export default class SearchSubSample extends Component<Props, State>{
    constructor(props) {
        super(props);
        this.state = ({searchingType: "name"})
    }
    render(){
        return(
            <div>
                {
                    this.state.searchingType === "name" &&
                    <SubSampleSearchByName />
                }
            </div>
        );
    }
}