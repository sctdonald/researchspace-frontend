//@flow

import React, {Component} from "react";
import UrlUtil from "../../../util/UrlUtil";
import type {SampleDetailInfo} from "../../../data/SampleModel";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import Config from "../../../env/Config";

type Props = {}
type State = {
    sampleDetail: SampleDetailInfo,
    currentSampleId: number,
    sampleIdSearch: number,
    isLoading: boolean
}

export default class SampleDetail extends Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = ({sampleDetail: null, isLoading: false});
        this.fetchSampleDetail = this.fetchSampleDetail.bind(this);
        this.onSampleIdChange = this.onSampleIdChange.bind(this);
        this.onSearchSampleDetailButtonClick = this.onSearchSampleDetailButtonClick.bind(this);
    }

    componentDidMount(): void {
        let queryParams = UrlUtil.getQueryParams();
        let sampleId = queryParams["id"];
        console.log(sampleId);
        this.setState({currentSampleId: sampleId, sampleIdSearch: sampleId})
        //this.fetchSampleDetail();
    }

    componentDidUpdate(prevProps: Props, prevState: State, prevContext: *): * {
        if (this.state.currentSampleId !== prevState.currentSampleId) {
            this.setState({sampleDetail: null, isLoading: true})
            this.fetchSampleDetail();
        }
    }

    fetchSampleDetail() {
        fetch(Config.researchBackend + "/v1/sample/" + this.state.sampleIdSearch + "/get-sample-detail",
            {
                method: "GET",
                headers: {
                    'Content-type': 'application/json',
                    'Access-Control-Request-Method': '*'
                }
            })
            .then(res => {
                if (!res.ok) {
                    this.setState({
                        isLoading: false,
                        sampleDetail: null,
                        error: "Inventory record with id " + this.state.sampleIdSearch + " could not be retrieved - possibly it has been deleted, does not exist, or you do not have permission to access it."
                    })
                }
                return res.json()
            })
            .then(
                (result) => {
                    if (!result.status) {
                        this.setState({
                            isLoading: false,
                            sampleDetail: result
                        });
                    }
                }
            ).catch(function (error) {                        // catch
            console.log('Request failed', error);
        })
    }

    onSampleIdChange(e) {
        this.setState({sampleIdSearch: e.target.value});
    }

    onSearchSampleDetailButtonClick() {
        window.location.href = "/sampleDetail?id=" + this.state.sampleIdSearch;
    }


    render() {
        return (
            <div>
                <div>
                    <a className="refLink" href={"/"}>Home</a>
                    <a className="refLink" href={"/sample"}>Search By User Name</a>
                </div>
                {
                    !this.state.currentSampleId &&
                    <div>
                        <div>You may search sample by id in the following box,</div>
                        <div><input type={"number"} onChange={this.onSampleIdChange}
                                        value={this.state.sampleIdSearch}/>
                            <button onClick={this.onSearchSampleDetailButtonClick}>go</button>
                        </div>
                    </div>
                }
                {
                    this.state.isLoading && <div>Loading...</div>
                }
                {
                    !this.state.isLoading && this.state.sampleDetail &&
                    <div className="sampleDetail">
                        <div>You are searching the sample detail with id: {this.state.currentSampleId}</div>
                        <div>ID: <input type={"number"} onChange={this.onSampleIdChange}
                                        value={this.state.sampleIdSearch}/>
                            <button onClick={this.onSearchSampleDetailButtonClick}>go</button>
                        </div>
                        {
                            !this.state.isLoading && this.state.error &&
                            <div>
                                We got an error when searching the result, the error message as follow:
                                <br/>
                                {this.state.error}
                            </div>
                        }
                        {
                            !this.state.isLoading && this.state.sampleDetail &&
                            <div>
                                <div>Global ID: {this.state.sampleDetail.globalId}</div>
                                <div>Name: {this.state.sampleDetail.name}</div>
                                <div>Description: {this.state.sampleDetail.description}</div>
                                <div>Created: {this.state.sampleDetail.created}</div>
                                <div>Created By: {this.state.sampleDetail.createdBy}</div>
                                <div>Last Modified: {this.state.sampleDetail.lastModified}</div>
                                <div>Modified By: {this.state.sampleDetail.modifiedBy}</div>
                                <div>Modified By Full Name: {this.state.sampleDetail.modifiedByFullName}</div>
                                <div>Deleted: {this.state.sampleDetail.deleted}</div>
                                <div>Deleted Date: {this.state.sampleDetail.deletedDate}</div>
                                {this.state.sampleDetail.owner &&
                                <div>
                                    <div>Owner Id: {this.state.sampleDetail.owner.id}</div>
                                    <div>Owner UserName: {this.state.sampleDetail.owner.username}</div>
                                    <div>Owner Full
                                        Name: {this.state.sampleDetail.owner.firstName} {this.state.sampleDetail.owner.lastName}</div>
                                </div>
                                }
                                <div>Permitted Actions: {this.state.sampleDetail.permittedActions.map(item => {
                                    return <div className="inlineText" key={item}>{item},</div>
                                })}</div>
                                <div>Tags: {this.state.sampleDetail.tags}</div>
                                <div>Barcode: {this.state.sampleDetail.barcode}</div>
                                <div>Type: {this.state.sampleDetail.type}</div>
                                <div>Version: {this.state.sampleDetail.version}</div>
                                <div>Historical Version: {this.state.sampleDetail.historicalVersion}</div>
                                <div>Template: {this.state.sampleDetail.template}</div>
                                {
                                    this.state.sampleDetail.template &&
                                    <div>
                                        <div>Template Id: {this.state.sampleDetail.templateId}</div>
                                        <div>Template Version: {this.state.sampleDetail.templateVersion}</div>
                                    </div>
                                }
                                <div>SubSample Alias: {this.state.sampleDetail.subSampleAlias}</div>
                                <div>SubSample Count: {this.state.sampleDetail.subSamplesCount}</div>
                                <div>Storage Temp Min: {this.state.sampleDetail.storageTempMin}</div>
                                <div>Storage Temp Max: {this.state.sampleDetail.storageTempMax}</div>
                                <div>Sample Source: {this.state.sampleDetail.sampleSource}</div>
                                <div>Expiry Date: {this.state.sampleDetail.expiryDate}</div>
                                <div>Fields: {this.state.sampleDetail.fields.map(item => {
                                    return <div className="inlineText" key={item}>{item}</div>
                                })}</div>
                                <div>Extra Fields: {this.state.sampleDetail.extraFields.map(item => {
                                    return <div className="inlineText" key={item}>{item}</div>
                                })}</div>
                                <div>SubSamples:</div>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>id</TableCell>
                                                <TableCell align="right">Global Id</TableCell>
                                                <TableCell align="right">Sample Name</TableCell>
                                                <TableCell align="right">Description</TableCell>
                                                <TableCell align="right">Created By</TableCell>
                                                <TableCell align="right">Created At</TableCell>
                                                <TableCell align="right">Version</TableCell>
                                                <TableCell align="right">Expiry Date</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {this.state.sampleDetail && this.state.sampleDetail.subSamples.map((row) => (
                                                <TableRow key={row.id}>
                                                    <TableCell component="th" scope="row"><a
                                                        href={"/subSampleDetail?id=" + row.id}>{row.id}</a></TableCell>
                                                    <TableCell align="right">{row.globalId}</TableCell>
                                                    <TableCell align="right">{row.name}</TableCell>
                                                    <TableCell align="right">{row.description}</TableCell>
                                                    <TableCell align="right">{row.createdBy}</TableCell>
                                                    <TableCell align="right">{row.created}</TableCell>
                                                    <TableCell align="right">{row.version}</TableCell>
                                                    <TableCell align="right">{row.expiryDate}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        }

                    </div>
                }

            </div>
        );
    }
}