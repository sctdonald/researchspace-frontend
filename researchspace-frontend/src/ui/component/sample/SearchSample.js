//@flow

import React, {Component} from 'react';
import SampleSearchByName from "./SampleSearchByName";

type State = {
    searchingType: string
}

type Props = {

}

export default class SearchSample extends Component<Props, State>{
    constructor(props) {
        super(props);
        this.state = ({searchingType: "name"})
    }
    render(){
        return(
            <div>
                {
                    this.state.searchingType === "name" &&
                        <SampleSearchByName />
                }
            </div>
        );
    }
}